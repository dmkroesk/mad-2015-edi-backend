var express = require('express');
var router = express.Router();
var objectreciever 	= 	require ('./services/objectreciever.js');
var organisations = 	require ('./services/organisations.js');
var buildings = 		require ('./services/buildings.js');
var contacts = 			require ('./services/contacts.js');
var rooms = 			require ('./services/rooms.js');
var sensors = 			require ('./services/sensors.js');
var measurements = 		require ('./services/measurements.js');

router.get('/api/ping', function(req, res){
		res.json({status: "OK"});
});

//Get data from database
router.get('/api/organisations', organisations.getOrganisations);
router.get('/api/organisations/:id', organisations.getOrganisationById);
router.get('/api/organisations/:id/buildings', buildings.getBuildingsByOrganisationId);

router.get('/api/buildings', buildings.getBuildings);
router.get('/api/buildings/:id', buildings.getBuildingById);
router.get('/api/buildings/:id/rooms', rooms.getRoomsByBuildingId);

router.get('/api/contacts', contacts.getContacts);
router.get('/api/contacts/:id', contacts.getContactsById);
router.get('/api/contacts/:id/buildings', buildings.getBuildingsByContactId);

router.get('/api/rooms', rooms.getRooms);
router.get('/api/rooms/:id', rooms.getRoomById);
router.get('/api/rooms/:id/sensors', sensors.getSensorsByRoom);

router.get('/api/sensors', sensors.getSensors);
router.get('/api/sensors/:id/measurements', measurements.getMeasurementsBySensorId);
router.get('/api/sensors/:id/lastmeasurement', measurements.getLastMeasurementBySensorId);
router.get('/api/sensors/:id/measurements/:hours', measurements.getMeasurementsBySensorIdAndCurrentTimestampHours);
router.get('/api/sensors/:id/measurements/:hours/:from', measurements.getMeasurementsBySensorIdAndFromTimestampTimespanHours);

router.get('/apiv2/sensors/:id/measurements', measurements.getMeasurementsBySensorIdv2);
router.get('/apiv2/sensors/:id/lastmeasurement', measurements.getLastMeasurementBySensorIdv2);
router.get('/apiv2/sensors/:id/measurements/:hours', measurements.getMeasurementsBySensorIdAndCurrentTimestampHoursv2);
router.get('/apiv2/sensors/:id/measurements/:hours/:from', measurements.getMeasurementsBySensorIdAndFromTimestampTimespanHoursv2);

//New data or update data in the database
router.post('/api/organisations', organisations.createOrganisation);
router.put('/api/organisations/:id', organisations.updateOrganisationById);

router.post('/api/buildings', buildings.createBuilding);
router.put('/api/buildings/:id', buildings.updateBuildingById);

router.post('/api/contacts', contacts.createContact);
router.put('/api/contacts/:id', contacts.updateContactsById);

router.post('/api/rooms', rooms.createRoom);
router.put('/api/rooms/:id', rooms.updateRoomById);

router.post('/api/sensors', sensors.createSensor);
router.put('/api/sensors/:id', sensors.updateSensorById);

router.post('/api/measurements', measurements.createMeasurement);
router.get('/api/measurements/export/:from/:to', measurements.createCSVExport);

//
module.exports = router;

