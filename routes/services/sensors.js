var sensors = {
	getSensors: function(req, res) {
		var query = "SELECT * FROM sensor;";
		objectreciever.getObject(req, res, query, "sensors");
	},
	getSensorById: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM sensor WHERE ID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "sensor");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getSensorsByRoom: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM sensor WHERE RoomID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "sensors");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	createSensor: function(req, res){
		var object = { 
			NodeID: req.body.NodeID || '',
			RoomID: req.body.RoomID || '',
			Deviceidentifier: req.body.Deviceidentifier || '',
			Name: req.body.Name || '',
			Type: req.body.Type || '',
			Description: req.body.Description || '',
			MapLocationX: req.body.MapLocationX || '',
			MapLocationY: req.body.MapLocationY || ''
		};
		objectreciever.createObject(req, res, object, "sensor");
	},
	updateSensorById: function(req, res){
		var object = {
			ID: req.params.id,
		};
		
		//set only values we want to update..
		if(req.body.NodeID) 					object['NodeID'] = req.body.NodeID;
		if(req.body.RoomID) 					object['RoomID'] = req.body.RoomID;
		if(req.body.Deviceidentifier) 			object['Deviceidentifier'] = req.body.Deviceidentifier;
		if(req.body.Name) 						object['Name'] = req.body.Name;
		if(req.body.Type) 						object['Type'] = req.body.Type;
		if(req.body.Description) 				object['Description'] = req.body.Description;
		if(req.body.MapLocationX) 				object['MapLocationX'] = req.body.MapLocationX;
		if(req.body.MapLocationY) 				object['MapLocationY'] = req.body.MapLocationY;

		objectreciever.updateObject(req, res, object, "sensor");
	}
};

module.exports = sensors;