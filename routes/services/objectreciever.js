objectreciever = { 
	MysqlConnection: function(app){
		var mysql = require('mysql');
		var settings = require('../../config.json');
		if(app.get('dbConnection') !== undefined){
			app.get('dbConnection').end();
		}
		var connection = mysql.createConnection(
		{
			host : settings.dbServer,
			user : settings.dbUsername,
			password : settings.dbPassword,
			database : settings.dbScheme
		});
		connection.connect();
		app.set('dbConnection', connection);
	},
	getObject: function(req, res, query, objectResult){
		if(objectResult === undefined){
			objectResult = "results";
		}
		var results = {};
		var db = req.app.get('dbConnection');
		db.query(query, function (err, rows, fields) {
			if(err){ 
				objectreciever.MysqlConnection(req.app);
				res.json({status: "ERROR"});
			}else{
				results[objectResult] = rows;
				res.json(results);
			}
		});
	},
	getSingleObject: function(req, res, query, callback){
		var db = req.app.get('dbConnection');
		db.query(query, function (err, rows, fields) {
			if(err){ 
				objectreciever.MysqlConnection(req.app);
				res.json({status: "ERROR"});
			}else if(callback !== undefined){
				callback(req, res, rows);	
			}
		});
	},
	createObject: function(req, res, object, tableName){
		var count = 0;
		var lengthObject = Object.keys(object).length;
		
		var keys = "";
		var values = "";
		var db = req.app.get('dbConnection');
		Object.keys(object).forEach(function(key) {
			count++;

		  	keys += key;
		  	values += db.escape(object[key]);
		  	
		  	if(lengthObject > count){
				keys+= ",";
				values+= ",";		  		
		  	}
		});

		var query = "INSERT INTO "+tableName+" ("+keys+") VALUES ("+values+");";
		var results = {};
		
		db.query(query, function (err, rows, fields) {
			if(err){ 
				objectreciever.MysqlConnection(req.app);
				res.json({status: "ERROR"}); 
			}else{
				res.json({status: "OK"});	
			}
		});
	},
	updateObject: function(req, res, object, tableName){
		if(object.ID && !isNaN(object.ID)){
			var count = 0;
			var lengthObject = Object.keys(object).length;
			
			var sets = "";
			var db = req.app.get('dbConnection');
			Object.keys(object).forEach(function(key) {
				count++;
				if(key !== "ID"){
				  	sets += key+"="+db.escape(object[key]);
				  	if(lengthObject > count){
						sets+= ",";
				  	}
			  	}
			});
			var query = "UPDATE "+tableName+" SET "+sets+" WHERE ID="+object.ID+";";

			var results = {};
			db.query(query, function (err, rows, fields) {
				if(err){ 
					objectreciever.MysqlConnection(req.app);
					res.json({status: "ERROR"}); 
				}else{
					res.json({status: "OK"});	
				}
			});
		}else{
			res.json({status: "ERROR, ID is not set"}); 	
		}
		
	}
}
module.exports = objectreciever;