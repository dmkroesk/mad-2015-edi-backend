var moment = require('moment');
var measurements = {
	getMeasurementsBySensorId: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT UNIX_TIMESTAMP(Timestamp) as 'Timestamp', Timestamp as 'TimestampConverted', SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "measurements");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getLastMeasurementBySensorId: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT UNIX_TIMESTAMP(Timestamp) as 'Timestamp', Timestamp as 'TimestampConverted', SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+" AND Timestamp >= CURRENT_TIMESTAMP() - INTERVAL 1 MINUTE ORDER BY Timestamp DESC LIMIT 0,1;";
			objectreciever.getObject(req, res, query, "measurement");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getMeasurementsBySensorIdAndCurrentTimestampHours: function(req, res) {
		if (!isNaN(req.params.id) && !isNaN(req.params.hours)) {
			var query = "SELECT UNIX_TIMESTAMP(Timestamp) as 'Timestamp', Timestamp as 'TimestampConverted', SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+" AND Timestamp >= CURRENT_TIMESTAMP() - INTERVAL "+req.params.hours+" HOUR;";
			objectreciever.getObject(req, res, query, "measurements");
		}else{
			res.json({error: "ID moet numeriek zijn. From moet een UnixTimestamp (seconden) zijn "});
		}
		
	},
	getMeasurementsBySensorIdAndFromTimestampTimespanHours: function(req, res) {
		if (!isNaN(req.params.id) && !isNaN(req.params.from) && !isNaN(req.params.hours)) {
			var query = "SELECT UNIX_TIMESTAMP(Timestamp) as 'Timestamp', Timestamp as 'TimestampConverted', SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+" AND (UNIX_TIMESTAMP(Timestamp) <= '"+moment(req.params.from, 'X').format("YYYY-MM-DD HH:mm:ss")+"' AND Timestamp >= CURRENT_TIMESTAMP() - INTERVAL "+req.params.hours+" HOUR;";
			objectreciever.getObject(req, res, query, "measurements");
		}else{
			res.json({error: "ID moet numeriek zijn. From moet een UnixTimestamp (seconden) zijn "});
		}
	},
	getMeasurementsBySensorIdv2: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT Timestamp, SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "measurements");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getLastMeasurementBySensorIdv2: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT Timestamp, SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+" AND Timestamp >= CURRENT_TIMESTAMP() - INTERVAL 1 MINUTE ORDER BY Timestamp DESC LIMIT 0,1;";
			objectreciever.getObject(req, res, query, "measurement");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getMeasurementsBySensorIdAndCurrentTimestampHoursv2: function(req, res) {
		if (!isNaN(req.params.id) && !isNaN(req.params.hours)) {
			var query = "SELECT Timestamp, SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+" AND Timestamp >= CURRENT_TIMESTAMP() - INTERVAL "+req.params.hours+" HOUR;";
			objectreciever.getObject(req, res, query, "measurements");
		}else{
			res.json({error: "ID moet numeriek zijn. From moet een UnixTimestamp (seconden) zijn "});
		}
		
	},
	getMeasurementsBySensorIdAndFromTimestampTimespanHoursv2: function(req, res) {
		if (!isNaN(req.params.id) && !isNaN(req.params.from) && !isNaN(req.params.hours)) {
			var query = "SELECT Timestamp, SensorID, Value, MeasurementUnit FROM measurement WHERE SensorID = "+req.params.id+" AND (UNIX_TIMESTAMP(Timestamp) <= '"+moment(req.params.from, 'X').format("YYYY-MM-DD HH:mm:ss")+"' AND Timestamp >= CURRENT_TIMESTAMP() - INTERVAL "+req.params.hours+" HOUR;";
			objectreciever.getObject(req, res, query, "measurements");
		}else{
			res.json({error: "ID moet numeriek zijn. From moet een UnixTimestamp (seconden) zijn "});
		}
	},
	createMeasurement: function (req, res){
		if(req.body.SensorDeviceidentifier !== undefined){
			objectreciever.getSingleObject(req, res, "SELECT * FROM sensor WHERE Deviceidentifier ='"+req.body.SensorDeviceidentifier+"'", measurements.callbackMeasurement);
		}else{
			measurements.callbackMeasurement(req, res);
		}
	},
	callbackMeasurement: function(req, res, results){
		var sensorIDdevice = '';
		if(results !== undefined && results.length !== 0 && results[0].ID !== undefined){
			sensorIDdevice = results[0].ID;
		}
		var object = { 
			SensorID: req.body.SensorID || sensorIDdevice,
			Value: req.body.Value || '',
			MeasurementUnit: req.body.MeasurementUnit || ''
		};
		objectreciever.createObject(req, res, object, "measurement");
	},
	createCSVExport: function(req, res){
		if (!isNaN(req.params.from) && !isNaN(req.params.to)) {
			
			var file = moment(new Date()).format('MMMM-Do-YYYY-h-mm-ss-a')+"-to-"+req.params.to+"-from-"+req.params.from+".csv";
			
			var to = moment(req.params.to, 'X');
			var from = moment(req.params.from, 'X');

			var query = "(SELECT 'Datum','Sensornaam','Waarde', 'Eenheid') UNION (SELECT m.Timestamp,s.Name, m.Value, m.MeasurementUnit FROM measurement m, sensor s WHERE s.ID=m.SensorID AND (m.Timestamp >= '"+from.format("YYYY-MM-DD HH:mm:ss")+"' AND m.Timestamp <= '"+to.format("YYYY-MM-DD HH:mm:ss")+"') INTO OUTFILE '/opt/mad-2015-edi-backend/www/"+file+"' FIELDS ENCLOSED BY '\"' TERMINATED BY ';' ESCAPED BY '\"' LINES TERMINATED BY '\\r\\n');";

			objectreciever.getSingleObject(req, res, query, function(req, res, results){
				res.json({'file': file});
			});
		}else{
			res.json({error: "From en TO moet een UnixTimestamp (seconden) zijn "});
		}
	}
};

module.exports = measurements;