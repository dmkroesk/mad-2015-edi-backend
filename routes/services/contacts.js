var contacts = {
	getContacts: function(req, res) {
		var query = "SELECT * FROM contact;";
		objectreciever.getObject(req, res, query, "contacts");
	},
	getContactsById: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM contact WHERE ID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "contact");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	createContact: function(req, res){
		var object = { 
			FirstName: req.body.FirstName || '',
			LastName: req.body.LastName || '',
			Adress: req.body.Adress || '',
			IsMale: req.body.IsMale || '',
			DateOfBirth: req.body.DateOfBirth || '',
			Title: req.body.Title || '',
			PhoneNumber: req.body.PhoneNumber || ''
		};
		
		objectreciever.createObject(req, res, object, "contact");
	},
	updateContactsById: function(req, res){
		var object = {
			ID: req.params.id,
		};
		
		//set only values we want to update..
		if(req.body.FirstName) 			object['FirstName'] = req.body.FirstName;
		if(req.body.LastName) 			object['LastName'] = req.body.LastName;
		if(req.body.Adress) 			object['Adress'] = req.body.Adress;
		if(req.body.IsMale) 			object['IsMale'] = req.body.IsMale;
		if(req.body.DateOfBirth) 		object['DateOfBirth'] = req.body.DateOfBirth;
		if(req.body.Title) 				object['Title'] = req.body.Title;
		if(req.body.PhoneNumber) 		object['PhoneNumber'] = req.body.PhoneNumber;

		objectreciever.updateObject(req, res, object, "contact");
	}
};

module.exports = contacts;