var organisations = {
	getOrganisations: function(req, res) {
		var query = "SELECT * FROM organisation;";
		objectreciever.getObject(req, res, query, "organisations");
	},
	getOrganisationById: function(req, res){
		if (!isNaN(req.params.id)) {
			var db = req.app.get('dbConnection');
			var query = "SELECT * FROM organisation WHERE ID = "+db.escape(req.params.id)+";";
			objectreciever.getObject(req, res, query, "organisation");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	createOrganisation: function(req, res){
		var object = { 
			Name: req.body.Name || '',
			Description: req.body.Description || ''
		};
		objectreciever.createObject(req, res, object, "organisation");
	},
	updateOrganisationById: function(req, res){
		var object = {
			ID: req.params.id,
		};
		
		//set only values we want to update..
		if(req.body.Name) 						object['Name'] = req.body.Name;
		if(req.body.Description) 				object['Description'] = req.body.Description;

		objectreciever.updateObject(req, res, object, "organisation");
	}
};

module.exports = organisations;