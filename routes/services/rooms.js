var rooms = {
	getRooms: function(req, res) {
		var query = "SELECT * FROM room;";
		objectreciever.getObject(req, res, query, "rooms");
	},
	getRoomById: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM room WHERE ID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "room");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getRoomsByBuildingId: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM room WHERE BuildingID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "rooms");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	createRoom: function(req, res){
		var object = { 
			BuildingID: req.body.BuildingID || '',
			Name: req.body.Name || '',
			Description: req.body.Description || '',
			MapPath: req.body.MapPath || '',
			Width: req.body.Width || '',
			Height: req.body.Height || ''
		};
		objectreciever.createObject(req, res, object, "room");
	},
	updateRoomById: function(req, res){
		var object = {
			ID: req.params.id,
		};
		
		//set only values we want to update..
		if(req.body.BuildingID) 		object['BuildingID'] = req.body.BuildingID;
		if(req.body.Name) 				object['Name'] = req.body.Name;
		if(req.body.Description)		object['Description'] = req.body.Description;
		if(req.body.MapPath) 			object['MapPath'] = req.body.MapPath;
		if(req.body.Width) 				object['Width'] = req.body.Width;
		if(req.body.Height) 			object['Height'] = req.body.Height;

		objectreciever.updateObject(req, res, object, "room");
	}
};

module.exports = rooms;