var objectreciever = require ('./objectreciever.js');

var buildings = {
	getBuildings: function(req, res) {
		var query = "SELECT * FROM building;";
		objectreciever.getObject(req, res, query, "buildings");
	},
	getBuildingById: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM building WHERE ID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "building");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getBuildingsByOrganisationId: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT * FROM building WHERE OrganisationID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "buildings");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	getBuildingsByContactId: function(req, res) {
		if (!isNaN(req.params.id)) {
			var query = "SELECT b.* FROM contact_building cb, building b WHERE cb.BuildingID = b.ID AND b.ID = "+req.params.id+";";
			objectreciever.getObject(req, res, query, "buildings");
		}else{
			res.json({error: "ID moet numeriek zijn."});
		}
	},
	createBuilding: function(req, res){
		var object = { 
			OrganisationID: req.body.OrganisationID || '',
			Name: req.body.Name || '',
			Street: req.body.Street || '',
			Postalcode: req.body.Postalcode || '',
			City: req.body.City || '',
			StreetNumber: req.body.StreetNumber || '',
			Description: req.body.Description || ''
		};
		
		objectreciever.createObject(req, res, object, "building");
	},
	updateBuildingById: function(req, res){
		var object = {
			ID: req.params.id,
		};
		
		//set only values we want to update..
		if(req.body.OrganisationID) 	object['OrganisationID'] = req.body.OrganisationID;
		if(req.body.Name) 				object['Name'] = req.body.Name;
		if(req.body.Street) 			object['Street'] = req.body.Street;
		if(req.body.Postalcode) 		object['Postalcode'] = req.body.Postalcode;
		if(req.body.City) 				object['City'] = req.body.City;
		if(req.body.StreetNumber) 		object['StreetNumber'] = req.body.StreetNumber;
		if(req.body.Description) 		object['Description'] = req.body.Description;

		objectreciever.updateObject(req, res, object, "building");
	}
};

module.exports = buildings;