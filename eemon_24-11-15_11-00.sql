# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.46-0ubuntu0.14.04.2)
# Database: eemondb
# Generation Time: 2015-11-24 10:00:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table building
# ------------------------------------------------------------

DROP TABLE IF EXISTS `building`;

CREATE TABLE `building` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `OrganisationID` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Street` varchar(255) DEFAULT NULL,
  `Postalcode` varchar(10) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `StreetNumber` varchar(10) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Organisation Building` (`OrganisationID`),
  CONSTRAINT `Organisation Building` FOREIGN KEY (`OrganisationID`) REFERENCES `organisation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `building` WRITE;
/*!40000 ALTER TABLE `building` DISABLE KEYS */;

INSERT INTO `building` (`ID`, `OrganisationID`, `Name`, `Street`, `Postalcode`, `City`, `StreetNumber`, `Description`)
VALUES
	(1,1,'Hogeschoollaan','Hogeschoollaan','4818 CR','Breda','1','Postbus 90.116, 4800 RA Breda'),
	(2,1,'Onderwijsboulevard 215','Onderwijsboulevard','5223 DE','\'s-Hertogenbosch','215','Postbus 90.116, 4800 RA Breda'),
	(3,1,'Professor Cobbenhagenlaan','Professor Cobbenhagenlaan','5037 DA','Tilburg','13','Postbus 90.116, 4800 RA Breda'),
	(4,1,'Lovensdijkstraat','Lovensdijkstraat','4818 AJ','Breda','61','Postbus 90.116, 4800 RA Breda'),
	(5,2,'Bergen op Zoom','Nobellaan','4622 AJ','Bergen op Zoom','50',NULL),
	(6,1,'Lovensdijkstraat','Lovensdijkstraat','4818 AJ','Breda','63','Postbus 90.116, 4800 RA Breda'),
	(7,2,'Tilburg','Apennijnenweg','5022 DT','Tilburg','9',NULL),
	(8,3,'NHTV Breda','Claudius Prinsenlaan','4811 DK','Breda','12',NULL);

/*!40000 ALTER TABLE `building` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Adress` varchar(255) DEFAULT NULL,
  `IsMale` bit(1) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Title` int(10) DEFAULT NULL,
  `PhoneNumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;

INSERT INTO `contact` (`ID`, `FirstName`, `LastName`, `Adress`, `IsMale`, `DateOfBirth`, `Title`, `PhoneNumber`)
VALUES
	(1,'Diederich','Kroeske','Straatnaam',10000000,'0000-00-00',0,'12345');

/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contact_building
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_building`;

CREATE TABLE `contact_building` (
  `ContactID` int(10) NOT NULL DEFAULT '0',
  `BuildingID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ContactID`,`BuildingID`),
  KEY `Building` (`BuildingID`),
  CONSTRAINT `Building` FOREIGN KEY (`BuildingID`) REFERENCES `building` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Contact` FOREIGN KEY (`ContactID`) REFERENCES `contact` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `contact_building` WRITE;
/*!40000 ALTER TABLE `contact_building` DISABLE KEYS */;

INSERT INTO `contact_building` (`ContactID`, `BuildingID`)
VALUES
	(1,1),
	(1,2),
	(1,3);

/*!40000 ALTER TABLE `contact_building` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SourceDevice` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `LogLevel` int(11) DEFAULT NULL,
  `Timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table measurement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `measurement`;

CREATE TABLE `measurement` (
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SensorID` int(11) NOT NULL DEFAULT '0',
  `Value` int(10) DEFAULT NULL,
  `MeasurementUnit` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Timestamp`,`SensorID`),
  KEY `Sensor Measurement` (`SensorID`),
  CONSTRAINT `Sensor Measurement` FOREIGN KEY (`SensorID`) REFERENCES `sensor` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `measurement` WRITE;
/*!40000 ALTER TABLE `measurement` DISABLE KEYS */;

INSERT INTO `measurement` (`Timestamp`, `SensorID`, `Value`, `MeasurementUnit`)
VALUES
	('2015-11-20 11:50:23',1,234,'W'),
	('2015-11-20 11:50:46',2,123,'W'),
	('2015-11-20 11:50:46',3,265,'W'),
	('2015-11-20 11:51:39',1,321,'W'),
	('2015-11-20 11:51:39',2,312,'W'),
	('2015-11-20 11:51:39',3,28,'W');

/*!40000 ALTER TABLE `measurement` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table node
# ------------------------------------------------------------

DROP TABLE IF EXISTS `node`;

CREATE TABLE `node` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BuildingID` int(11) DEFAULT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Name` varchar(256) DEFAULT NULL,
  `ShortName` varchar(16) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `SoftwareVersion` varchar(255) DEFAULT NULL,
  `DeviceIdentifier` varchar(255) DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Building Node` (`BuildingID`),
  CONSTRAINT `Building Node` FOREIGN KEY (`BuildingID`) REFERENCES `building` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `node` WRITE;
/*!40000 ALTER TABLE `node` DISABLE KEYS */;

INSERT INTO `node` (`ID`, `BuildingID`, `Timestamp`, `Name`, `ShortName`, `Description`, `SoftwareVersion`, `DeviceIdentifier`, `Longitude`, `Latitude`)
VALUES
	(1,6,'2015-11-20 11:43:03','Raspberry Pi Prototype','Rpi','Z-Wave','0.1','Awesome',51.24,54.12);

/*!40000 ALTER TABLE `node` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisation`;

CREATE TABLE `organisation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL DEFAULT '',
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `organisation` WRITE;
/*!40000 ALTER TABLE `organisation` DISABLE KEYS */;

INSERT INTO `organisation` (`ID`, `Name`, `Description`)
VALUES
	(1,'Avans Hogeschool','Avans Hogeschool is een opleidingsinstituut voor hoger onderwijs in de Nederlandse provincie Noord-Brabant en heeft vestigingen in Breda, \'s-Hertogenbosch en Tilburg.'),
	(2,'Fontys Hogescholen','Fontys Hogescholen is de naam van een Nederlandse instelling voor hoger beroepsonderwijs met de statutaire zetel in Tilburg. Internationaal wordt de naam Fontys University of Applied Sciences gevoerd.'),
	(3,'NHTV internationaal hoger onderwijs Breda','NHTV internationaal hoger onderwijs Breda is een middelgroot opleidingsinstituut voor hoger onderwijs gevestigd in Breda. Aan de instelling studeren circa 7000 Nederlandse en buitenlandse studenten.');

/*!40000 ALTER TABLE `organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table room
# ------------------------------------------------------------

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BuildingID` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `MapPath` varchar(255) DEFAULT NULL,
  `Width` float DEFAULT NULL,
  `Height` float DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Building Room` (`BuildingID`),
  CONSTRAINT `Building Room` FOREIGN KEY (`BuildingID`) REFERENCES `building` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;

INSERT INTO `room` (`ID`, `BuildingID`, `Name`, `Description`, `MapPath`, `Width`, `Height`)
VALUES
	(1,6,'LA210','Leraren kamer','http://www.',390,390);

/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sensor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sensor`;

CREATE TABLE `sensor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NodeID` int(11) DEFAULT NULL,
  `RoomID` int(11) DEFAULT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Deviceidentifier` varchar(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `MapLocationX` float DEFAULT NULL,
  `MapLocationY` float DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Sensor Node` (`NodeID`),
  KEY `Sensor Room` (`RoomID`),
  CONSTRAINT `Sensor Node` FOREIGN KEY (`NodeID`) REFERENCES `node` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Sensor Room` FOREIGN KEY (`RoomID`) REFERENCES `room` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;

INSERT INTO `sensor` (`ID`, `NodeID`, `RoomID`, `Timestamp`, `Deviceidentifier`, `Name`, `Type`, `Description`, `MapLocationX`, `MapLocationY`)
VALUES
	(1,1,1,'2015-11-20 11:46:57','ABCD-EER-AA','Stekker 1','PLUG','Testing..',10,20),
	(2,1,1,'2015-11-20 11:46:57','ABCD-EER-AB','Stekker 2','PLUG','AAA',20,30),
	(3,1,1,'2015-11-20 11:46:57','ABCD-EER-AC','Stekker 3','PLUG','ASDASD',50,20);

/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
